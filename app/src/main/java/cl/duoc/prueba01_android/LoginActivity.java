package cl.duoc.prueba01_android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cl.duoc.prueba01_android.BD.ColeccionUsuario;
import cl.duoc.prueba01_android.Entidades.Usuario;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etUsuario,etContraseña;
    private Button btnLogin,btnRegistrarse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsuario = (EditText) findViewById(R.id.etUsuarioLogin);
        etContraseña = (EditText) findViewById(R.id.etContraseñaLogin);

        btnLogin =(Button) findViewById(R.id.btnIngresar);
        btnRegistrarse =(Button) findViewById(R.id.btnRegistrarse);

        btnLogin.setOnClickListener(this);
        btnRegistrarse.setOnClickListener(this);


    }

    private void Ingresar() {
        for (Usuario user : ColeccionUsuario.obtieneListadoUsuarios()){
            if (etUsuario.getText().toString().equals(user.getNombre())&&etContraseña.getText().toString().equals(user.getClave())){
                Toast.makeText(this, "Usuario Valido", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(LoginActivity.this,PerfilActivity.class);
                startActivity(intent);
            }
        }
        Toast.makeText(this, "Usuario Invalido", Toast.LENGTH_SHORT).show();
        etUsuario.setText("");
        etContraseña.setText("");
        etUsuario.requestFocus();
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.btnIngresar){
                    Ingresar();
        }
        else if(v.getId()==R.id.btnRegistrarse)
        {
            Intent intent = new Intent(LoginActivity.this,RegistrarseActivity.class);
            startActivity(intent);
        }
    }
}
