package cl.duoc.prueba01_android.BD;

import java.util.ArrayList;

import cl.duoc.prueba01_android.Entidades.Usuario;

/**
 * Created by DUOC on 20-04-2017.
 */

public class ColeccionUsuario {
    private static ArrayList<Usuario> values = new ArrayList<>();
    public static void agregarUsuario(Usuario usuario){
        values.add(usuario);
    }

    public static ArrayList<Usuario> obtieneListadoUsuarios(){
        return values;
    }
}
