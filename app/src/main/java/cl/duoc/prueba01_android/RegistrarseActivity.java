package cl.duoc.prueba01_android;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cl.duoc.prueba01_android.BD.ColeccionUsuario;
import cl.duoc.prueba01_android.Entidades.Usuario;

public class RegistrarseActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etUsuario, etContra1, etContra2;
    private Button btnRegistrarse,btnVolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrarse);

        showToolbar(getResources().getString(R.string.toolbar_tittle_create),true);


        etUsuario = (EditText) findViewById(R.id.etNombreUsuarioRegistrarse);
        etContra1 = (EditText) findViewById(R.id.etContraseñaUsuarioRegistrarse);
        etContra2 = (EditText) findViewById(R.id.etRepContraseñaUsuarioRegistrarse);

        btnRegistrarse = (Button) findViewById(R.id.btnRegistrarseRegistro);


        btnRegistrarse.setOnClickListener(this);

    }
    public void showToolbar(String titulo,Boolean upButton){
        Toolbar tol=(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(tol);
        getSupportActionBar().setTitle(titulo);
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.btnRegistrarseRegistro){
            String mensajeError="";
            if (etContra1.getText().toString().length() <= 0){
                mensajeError += " Error Contraseña1 \n";
            }else if (etContra1.getText().toString().length() <= 0){
                mensajeError += " Error Contraseña1 \n";
            }else if (!etContra1.getText().toString().equals(etContra2.getText().toString())){
                mensajeError += " Contraseñas Distintas \n";
            }else if (etUsuario.getText().toString().length() <= 0) {
                mensajeError += " Error Usuario \n";
            } else {
                Usuario user = new Usuario();
                user.setNombre(etUsuario.getText().toString());
                user.setClave(etContra1.getText().toString());
                Toast.makeText(this, "Usuario Registrado", Toast.LENGTH_LONG).show();

                etUsuario.setText("");
                etContra1.setText("");
                etContra2.setText("");
                ColeccionUsuario.agregarUsuario(user);
            }
            Toast.makeText(this,mensajeError, Toast.LENGTH_LONG).show();

        }
    }
}
