package cl.duoc.prueba01_android;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import cl.duoc.prueba01_android.BD.ColeccionUsuario;

public class ListadoUsuariosActivity extends AppCompatActivity implements View.OnClickListener{


    private ListView lVListadoUsuarios;
    private Button btnVolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar);


        UsuarioAdapter adaptador =new UsuarioAdapter(this, ColeccionUsuario.obtieneListadoUsuarios());
        lVListadoUsuarios = (ListView) findViewById(R.id.listViewListaUsuarios);
        lVListadoUsuarios.setAdapter(adaptador);

        btnVolver = (Button) findViewById(R.id.btnVolverListar);
        btnVolver.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.btnVolverListar){
            finish();
        }
    }
}
