package cl.duoc.prueba01_android;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import cl.duoc.prueba01_android.Entidades.Usuario;

/**
 * Created by DUOC on 20-04-2017.
 */

public class UsuarioAdapter extends ArrayAdapter<Usuario>{

    private ArrayList<Usuario> dataSource;

    public UsuarioAdapter(Context context, ArrayList<Usuario> dataSource) {
        super(context, R.layout.item_listar_usuario, dataSource);
        this.dataSource = dataSource;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View item = inflater.inflate(R.layout.item_listar_usuario, null);
        TextView tvUsuariosListar = (TextView)item.findViewById(R.id.tvUsuarioListar);
        tvUsuariosListar.setText(dataSource.get(position).getNombre());
        return(item);
    }

}
